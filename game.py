import json

from datetime import datetime

from random import randint

from result import Result

playing = True
hard_mode = False
level = 0
doors = 5
start = 0

# Loading recent games from file.
with open(file='results.json', encoding='UTF-8') as json_file:
    data = json.load(json_file)

people = data['people']


def print_startup_information():
    # Printing information about the game
    print('------------------------------')
    print('GhostGame is starting now...\n')
    print('Author           : Sophie Gromann & Lars Lewerenz')
    print('Version          : 1.0')
    print('Last edit        : 10/14/2019')
    print('Played times     : ' + str(len(people)))


print_startup_information()
# setting default entries

best_easy_result = Result('Lars Lewerenz', '10b', -1, -1, -1)
best_hard_result = Result('Sophie Gromann', '10b', -1, -1, -1)


def find_best_results():
    global best_hard_result
    global best_easy_result
    for person in people:
        best_name = person['name']
        best_class_name = person['class_name']
        best_timestamp = person['timestamp']
        best_hard = person['hard_mode']
        best_level = person['level']
        if best_hard:
            if best_level > best_hard_result.level:
                best_hard_result = Result(best_name, best_class_name, best_timestamp, best_hard, best_level)
        else:
            if best_level > best_easy_result.level:
                best_easy_result = Result(best_name, best_class_name, best_timestamp, best_hard, best_level)


def print_best_results():
    if best_hard_result.timestamp != -1:
        print('Highscore (hard) : ' + str(best_hard_result.level) + ' '
              + '(' + best_hard_result.name + ', ' + best_hard_result.class_name
              + ' | ' + ('{:%d.%m.%Y %H:%M:%S}'.format(datetime.fromtimestamp(best_hard_result.timestamp))) + ')')
    else:
        print('Highscore (hard) : Not defined yet')
    if best_easy_result.timestamp != -1:
        print('Highscore (easy) : ' + str(best_easy_result.level) + ' '
              + '(' + best_easy_result.name + ', ' + best_easy_result.class_name
              + ' | ' + ('{:%d.%m.%Y %H:%M:%S}'.format(datetime.fromtimestamp(best_easy_result.timestamp))) + ')')
    else:
        print('Highscore (easy) : Not defined yet')


def print_result():
    # Printing the result!
    print('---------------------------')
    print('\nThe game is over!')
    result.print_information()
    print('Ranking    : #' + '1')
    print('---------------------------')


def append_data():
    # Appending new data.
    people.append({
        'name': result.name,
        'class_name': result.class_name,
        'hard_mode': result.hard,
        'timestamp': result.timestamp,
        'level': result.level
    })


def save_data():
    # Saving data to file here.
    with open(file='results.json', mode='w', encoding='UTF-8') as outfile:
        json.dump(data, outfile, indent=1, ensure_ascii=False)


def append_and_save_data():
    append_data()
    save_data()


def print_explanation():
    print('\nIn front of you there are ' + str(doors) + ' doors.')
    print('You can choose between 1 or ' + str(doors) + '!')
    print('There\'s a ghost behind a door. Try not to meet him, because you\'r afraid of it.\n')


def read_mode():
    mode = input('Please enter the mode you want to play (easy, hard):\n')
    global hard_mode
    global doors
    if mode.lower() == 'hard':
        hard_mode = True
        doors = 3

    if hard_mode:
        print('Ohh, you try the hard mode! Have fun :)')
    else:
        print('Ah, you take the easier way :D Have fun anyway :)')


def save_startup():
    # Timestamp when the game starts.
    global start
    now = datetime.now()
    start = datetime.timestamp(now)


def increment_level():
    global level
    level = level + 1


def create_random_number(minimum, limit):
    return randint(minimum, limit)


# Main

find_best_results()
print_best_results()
print('------------------------------\n')

# Letting person enter the name.
name = input('Please enter your name:\n')

# Letting person enter the class.
class_name = input('Please enter your class:\n')

# Read mode by letting enter it
read_mode()

save_startup()

while playing:
    print_explanation()
    input_number = input('Which one do you want to choose?\n')
    try:
        chosen_door = int(input_number)
    except ValueError:
        print('Enter a valid number, please!')
        continue
    if chosen_door > doors or chosen_door < 1:
        print('You can only choose between 1 to ' + str(doors) + '!')
        continue
    # Compute random door for ghost.
    ghost_door = create_random_number(1, doors)
    if chosen_door == ghost_door:
        print('Oh no! There was a ghost!')
        playing = False
        break
    print('Yes! There was no ghost!\n')
    increment_level()
# Defining a new entry for this game.
result = Result(name, class_name, start, hard_mode, level)

print_result()
append_and_save_data()
