from datetime import datetime


class Result:

    def __init__(self, name, class_name, timestamp, hard, level):
        self.name = name
        self.class_name = class_name
        self.timestamp = timestamp
        self.hard = hard
        self.level = level

    def print_information(self):
        print("Name       : " + self.name)
        print('Class      : ' + str(self.class_name))
        print('Date       : ' + '{:%d.%m.%Y %H:%M:%S}'.format(datetime.fromtimestamp(self.timestamp)))
        print('Hard mode  : ' + str(self.hard))
        print('Level      : ' + str(self.level))
